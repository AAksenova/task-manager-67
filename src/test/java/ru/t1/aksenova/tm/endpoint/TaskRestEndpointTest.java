package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.client.TaskRestEndpointClient;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.marker.IntegrationCategory;

import java.util.Collections;
import java.util.List;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @NotNull
    final static TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    private static final TaskDTO taskOne = new TaskDTO(ALFA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final TaskDTO taskTwo = new TaskDTO(BETTA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final TaskDTO taskThree = new TaskDTO(GAMMA_TASK_NAME, Status.valueOf(STATUS_NOT_STARTED));


    @BeforeClass
    public static void setUp() {
        client.add(taskOne);
        client.add(taskTwo);
    }

    @AfterClass
    public static void tearDown() {
        client.delete(taskOne);
        client.delete(taskTwo);
        client.delete(taskThree);
    }

    @Test
    public void add() {
        @NotNull final String taskName = taskThree.getName();
        @Nullable final TaskDTO task = client.add(taskThree);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getName());
    }

    @Test
    public void save() {
        @Nullable TaskDTO task = client.findById(taskTwo.getId());
        Assert.assertNotNull(task);
        task.setDescription(BETTA_TASK_DESCRIPTION);
        task.setStatus(Status.valueOf(STATUS_IN_PROGRESS));
        client.save(task);
        @Nullable TaskDTO task2 = client.findById(taskTwo.getId());
        Assert.assertNotNull(task2);
        Assert.assertEquals(task.getDescription(), task2.getDescription());
        Assert.assertEquals(task.getStatus(), task2.getStatus());
    }

    @Test
    public void findAll() {
        final long taskSize = client.count();
        @Nullable final List<TaskDTO> tasks = client.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskSize, tasks.size());
        tasks.forEach(task -> Assert.assertNotNull(task.getId()));
    }

    @Test
    public void findById() {
        @NotNull final String taskId = taskOne.getId();
        @Nullable final TaskDTO task = client.findById(taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskId, task.getId());
    }

    @Test
    public void delete() {
        Assert.assertNotNull(client.findById(taskOne.getId()));
        client.delete(taskOne);
        Assert.assertThrows(Exception.class, () -> client.findById(taskOne.getId()));
        client.add(taskOne);
    }

    @Test
    public void deleteById() {
        @NotNull final String taskId = taskOne.getId();
        Assert.assertNotNull(client.findById(taskId));
        client.deleteById(taskId);
        Assert.assertThrows(Exception.class, () -> client.findById(taskId));
        client.add(taskOne);
    }

    @Test
    public void count() {
        final long taskSize = client.count();
        @Nullable final List<TaskDTO> tasks = client.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskSize, tasks.size());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(client.existsById(taskTwo.getId()));
        @NotNull final TaskDTO taskFour = new TaskDTO(DELTA_TASK_NAME, Status.valueOf(STATUS_COMPLETE));
        Assert.assertThrows(Exception.class, () -> client.existsById(taskFour.getId()));
    }

    @Test
    public void clear() {
        @Nullable final List<TaskDTO> tasks = client.findAll();
        Assert.assertNotNull(tasks);
        client.clear();
        @Nullable final List<TaskDTO> tasks2 = client.findAll();
        Assert.assertNotNull(tasks2);
        Assert.assertEquals(Collections.emptyList(), tasks2);
        Assert.assertEquals(0, tasks2.size());
        for (@NotNull TaskDTO task : tasks) {
            client.add(task);
        }
    }

}
