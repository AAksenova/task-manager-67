package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.aksenova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.aksenova.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/add")
    public ProjectDTO add(
            @WebParam(name = "project", partName = "project")
            @RequestBody final @NotNull ProjectDTO project
    ) {
        return projectService.add(project);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final @NotNull ProjectDTO project
    ) {
        projectService.update(project);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) {
        return projectService.findOneById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) {
        ProjectDTO p = projectService.findOneById(id);
        return (projectService.findOneById(id) != null);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectService.getSize();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) {
        projectService.removeOneById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody final @NotNull ProjectDTO project
    ) {
        projectService.remove(project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void clear() {
        projectService.clear();
    }

}
