package ru.t1.aksenova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.service.model.ITaskService;
import ru.t1.aksenova.tm.entity.model.Task;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.entity.TaskNotFoundException;
import ru.t1.aksenova.tm.exception.field.*;
import ru.t1.aksenova.tm.repository.model.TaskRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService implements ITaskService {

    @Nullable
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Override
    public TaskRepository getRepository() {
        return repository;
    }

    @Override
    @NotNull
    @Transactional
    public Task add(@NotNull final Task task) {
        getRepository().save(task);
        return task;
    }

    @Override
    @Transactional
    public void update(@NotNull Task task) {
        getRepository().save(task);
    }

    @Override
    @Transactional
    public void remove(@NotNull Task task) {
        getRepository().delete(task);
    }

    @NotNull
    @Override
    public Task create(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        getRepository().save(task);
        return task;
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        getRepository().save(task);
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> set(@NotNull Collection<Task> tasks) {
        if (tasks.isEmpty()) return Collections.emptyList();
        tasks.forEach(getRepository()::save);
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public Task findOneById(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Optional<Task> task = getRepository().findById(id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        return task.orElse(null);
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().deleteAll();
    }

    @Override
    @Transactional
    public void removeAll() {
        getRepository().deleteAll();
    }

    @Nullable
    @Override
    public Task removeOne(
            @Nullable final Task task
    ) {
        if (task == null) throw new TaskNotFoundException();
        remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneById(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(id);
        remove(task);
        return task;
    }

    @Override
    @NotNull
    public void removeByProjectId(
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        getRepository().deleteByProjectId(projectId);
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return (getRepository().findById(id).orElse(null) != null);
    }

    @Override
    public long getSize() {
        return getRepository().count();
    }

}
