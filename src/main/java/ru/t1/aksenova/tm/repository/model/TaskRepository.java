package ru.t1.aksenova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.entity.model.Task;

import javax.transaction.Transactional;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface TaskRepository extends JpaRepository<Task, String> {

    @Transactional
    void deleteByProjectId(@NotNull String projectId);

}
