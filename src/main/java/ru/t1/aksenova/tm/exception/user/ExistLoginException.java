package ru.t1.aksenova.tm.exception.user;

public final class ExistLoginException extends AbstractUserException {

    public ExistLoginException() {
        super("Error! This login already exists...");
    }

    public ExistLoginException(String message) {
        super("Error! This login \"" + message + "\" already exists...");
    }


}
