package ru.t1.aksenova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends IDatabaseProperty {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDatabaseInitToken();

    @NotNull
    String getLiquibasePropertiesFilename();

}
