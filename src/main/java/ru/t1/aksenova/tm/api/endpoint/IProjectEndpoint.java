package ru.t1.aksenova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @NotNull
    @WebMethod
    @PostMapping("/add")
    ProjectDTO add(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull ProjectDTO project
    );

    @WebMethod
    @GetMapping("/findAll")
    List<ProjectDTO> findAll();

    @WebMethod
    @PostMapping("/save")
    void save(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull ProjectDTO project
    );

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull ProjectDTO project
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void clear();

}
