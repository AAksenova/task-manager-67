package ru.t1.aksenova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    @PostMapping("/add")
    TaskDTO add(
            @WebParam(name = "task", partName = "task")
            @RequestBody @NotNull TaskDTO task
    );

    @WebMethod
    @GetMapping("/findAll")
    List<TaskDTO> findAll();

    @WebMethod
    @PostMapping("/save")
    void save(
            @WebParam(name = "task", partName = "task")
            @RequestBody @NotNull TaskDTO task
    );

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody @NotNull TaskDTO task
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void clear();

}
