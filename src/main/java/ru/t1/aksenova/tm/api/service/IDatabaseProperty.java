package ru.t1.aksenova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseConnection();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseHibernateDialect();

    @NotNull
    String getDatabaseHibernateHbm2ddl();

    @NotNull
    String getDatabaseHibernateShowSql();

    @NotNull
    String getDatabaseHibernateFormatSql();

    @NotNull String getDatabaseCacheUseSecondLevelCache();

    @NotNull String getDatabaseCacheUseMinimalPuts();

    @NotNull String getDatabaseCacheUseQueryCache();

    @NotNull String getDatabaseCacheRegionPrefix();

    @NotNull String getDatabaseCacheProviderConfigFileResourcePath();

    @NotNull String getDatabaseCacheRegionFactoryClass();
}