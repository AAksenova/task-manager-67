package ru.t1.aksenova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.entity.model.Task;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.repository.model.TaskRepository;

import java.util.Collection;
import java.util.List;

public interface ITaskService {
    @NotNull TaskRepository getRepository();

    @NotNull
    @Transactional
    Task add(@NotNull Task task);

    @Transactional
    void update(@NotNull Task task);

    @Transactional
    void remove(@NotNull Task task);

    @NotNull Task create(@Nullable Task task);

    @NotNull Task create(
            @Nullable String name,
            @Nullable String description
    );

    @NotNull Collection<Task> set(@NotNull Collection<Task> tasks);

    @NotNull List<Task> findAll();

    @Nullable Task findOneById(
            @Nullable String id
    );

    @Transactional
    void clear();

    @Transactional
    void removeAll();

    @Nullable Task removeOne(
            @Nullable Task task
    );

    @Nullable Task removeOneById(
            @Nullable String id
    );

    @NotNull void removeByProjectId(
            @Nullable String projectId
    );

    @NotNull Task updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull Task changeTaskStatusById(
            @Nullable String id,
            @Nullable Status status
    );

    boolean existsById(@Nullable String id);

    long getSize();
}
